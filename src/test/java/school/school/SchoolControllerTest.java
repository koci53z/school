package school;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import school.controller.printer.PrinterMVC;
import school.controller.scanner.ScannerMVC;
import school.model.School;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.atMostOnce;

class SchoolControllerTest {

    PrinterMVC.Controller printerController = mock(PrinterMVC.Controller.class);
    ScannerMVC.Controller scannerController = mock(ScannerMVC.Controller.class);
    SchoolMVC.Model school = mock(School.class);
    SchoolMVC.Controller tested = new SchoolController(school, scannerController, printerController);
    SchoolMVC.Model schoolNotMock = new School();
    SchoolMVC.Controller testedNotMock = new SchoolController(schoolNotMock, scannerController, printerController);

    @BeforeEach
    public void reset() {
        schoolNotMock.reset();
    }

    @Test
    public void menuPrintTest() {
        when(scannerController.pickOption()).thenReturn(8);
        tested.manageSchool();
        verify(printerController, times(9)).println(anyString());
    }

    @Test
    public void addClassTest() {
        when(scannerController.pickOption()).thenReturn(1).thenReturn(8);
        when(scannerController.nextString()).thenReturn("I");
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj nazwę klasy");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
    }

    @Test
    public void addStudentTest() {
        when(scannerController.pickOption()).thenReturn(1).thenReturn(2)
                .thenReturn(8);
        when(scannerController.nextString()).thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1");
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
    }

    @Test
    public void addSubjectTest() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
    }

    @Test
    public void addSubjectToStudentTest() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Uczniowie");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());
    }

    @Test
    public void addGradeToStudentTest() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        verify(printerController, times(2)).println("Uczniowie");
        verify(printerController, times(1)).println("Podaj ID ucznia");
        verify(printerController, times(1)).println("Przedmioty ucznia");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ocenę");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());

        verify(printerController, times(2)).println(schoolNotMock.getStudentDAO().students);
        verify(printerController, times(1)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));
        assertEquals(1, schoolNotMock.getGradeDAO().getStudentGradesOfSubject(1).get(0));
        assertEquals(1, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade);

    }

    @Test
    public void correctGradeTest1() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(6).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5);
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        verify(printerController, times(3)).println("Uczniowie");
        verify(printerController, times(2)).println("Podaj ID ucznia");
        verify(printerController, times(2)).println("Przedmioty ucznia");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ocenę");
        verify(printerController, times(1)).println("Podaj ID przedmiotu");
        verify(printerController, times(1)).println("Oceny z przedmiotu");
        verify(printerController, times(1)).println("Podaj ID oceny do poprawy");
        verify(printerController, times(1)).println("Podaj nową ocenę");
        verify(printerController, times(1)).println("Ocena zmieniona");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());

        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().students);
        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().getStudentsList());
        verify(printerController, times(2)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));

        assertEquals(3, schoolNotMock.getGradeDAO().getStudentGradesOfSubject(1).get(0));
        assertEquals(3, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade);
        assertTrue(schoolNotMock.getGradeDAO().getStudentGradeById(1).get().isCorrected);

    }

    @Test
    public void correctGradeTest2() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(6).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(4).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5);
        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        verify(printerController, times(3)).println("Uczniowie");
        verify(printerController, times(2)).println("Podaj ID ucznia");
        verify(printerController, times(2)).println("Przedmioty ucznia");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ocenę");
        verify(printerController, times(1)).println("Podaj ID przedmiotu");
        verify(printerController, times(1)).println("Oceny z przedmiotu");
        verify(printerController, times(1)).println("Podaj ID oceny do poprawy");
        verify(printerController, times(1)).println("Podaj nową ocenę");
        verify(printerController, times(1)).println("Ocena poprawiona lub wyższa niż 1");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());

        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().students);
        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().getStudentsList());
        verify(printerController, times(2)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));

        assertEquals(4, schoolNotMock.getGradeDAO().getStudentGradesOfSubject(1).get(0));
        assertEquals(4, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade);
        assertFalse(schoolNotMock.getGradeDAO().getStudentGradeById(1).get().isCorrected);

    }

    @Test
    public void endSchoolTest1() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(7).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5).thenReturn(1);

        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        verify(printerController, times(2)).println("Uczniowie");
        verify(printerController, times(1)).println("Podaj ID ucznia");
        verify(printerController, times(1)).println("Przedmioty ucznia");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ocenę");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());

        assertEquals(1, schoolNotMock.getGradeDAO().gradeList.size());
        assertEquals(1, schoolNotMock.getStudentDAO().getStudentsList().size());
    }

    @Test
    public void endSchoolTest2() {
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(7).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);

        testedNotMock.manageSchool();
        verify(printerController, times(1)).println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
        verify(printerController, times(1)).println("Aktualnie dostępne klasy:");
        verify(printerController, times(1)).println("Podaj nazwę przedmiotu");
        verify(printerController, times(1)).println("Adtualne przedmioty");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ID ucznia");
        verify(printerController, times(2)).println("Uczniowie");
        verify(printerController, times(1)).println("Podaj ID ucznia");
        verify(printerController, times(1)).println("Przedmioty ucznia");
        verify(printerController, times(1)).println("Podaj ID przedmiotu oraz ocenę");
        verify(printerController, times(1)).println("Podaj poprawioną ocenę");
        verify(printerController, times(1)).println("Srednia niższa niż 4.3, uczeń zostanie usunięty");
        assertEquals(1, schoolNotMock.getStudentClassDAO().studentClassList.size());
        assertEquals(0, schoolNotMock.getStudentClassDAO().studentToStudentClassList.size());
        assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectList().size());
        assertEquals(1, schoolNotMock.getSubjectDAO().subjects.size());
        assertEquals(0, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size());
        assertEquals(0, schoolNotMock.getSubjectDAO().subjectsToStudentsList.size());

        assertEquals(0, schoolNotMock.getGradeDAO().gradeList.size());
        assertEquals(0, schoolNotMock.getStudentDAO().getStudentsList().size());
    }


}