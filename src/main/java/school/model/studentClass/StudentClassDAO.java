package school.model.studentClass;

import school.model.StudentToStudentClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StudentClassDAO implements StudentClassInterface {

    public List<StudentClass> studentClassList = new ArrayList<>();
    public List<StudentToStudentClass> studentToStudentClassList = new ArrayList<>();

    public void addStudentToClass(int studentId, int classId) {

        if (studentToStudentClassList.stream().noneMatch(p -> p.studentId == studentId)) {

            StudentToStudentClass studentToStudentClass = new StudentToStudentClass();

            if (studentToStudentClassList.isEmpty()) {
                studentToStudentClass.studentToClassId = 1;
            } else {
                studentToStudentClass.studentToClassId = studentToStudentClassList
                        .get(studentToStudentClassList.size() - 1).studentToClassId + 1;
            }

            studentToStudentClass.studentId = studentId;
            studentToStudentClass.classId = classId;

            studentToStudentClassList.add(studentToStudentClass);
        } else {
            System.out.println("student already in calss");
        }

    }

    public void addStudentClass(String name) {

        StudentClass studentClass = new StudentClass();

        studentClass.studentClassName = name;
        if (studentClassList.isEmpty()) {
            studentClass.studentClassId = 1;
        } else {
            studentClass.studentClassId = studentClassList.get(studentClassList.size() - 1).studentClassId + 1;
        }

        studentClassList.add(studentClass);
    }

    public void removeStudentClass(int classId) {
        studentClassList = studentClassList.stream()
                .filter(studentClass -> studentClass.studentClassId != classId)
                .collect(Collectors.toList());
    }

    public void removeStudentFromStudentClass(int studentId) {
        studentToStudentClassList = studentToStudentClassList.stream()
                .filter(studentToStudentClass -> studentToStudentClass.studentId != studentId)
                .collect(Collectors.toList());
    }

    public Optional<StudentClass> getStudentClassById(int classId) {
        return studentClassList.stream().filter(clazz -> clazz.studentClassId == classId).findFirst();
    }

    public List<Integer> getStudentsOfClassById(int classId) {
        return studentToStudentClassList.stream()
                .filter(clazz -> clazz.classId == classId)
                .map(p -> p.studentId)
                .collect(Collectors.toList());
    }

    public void resetDAO() {
        studentClassList = new ArrayList<>();
        studentToStudentClassList = new ArrayList<>();
    }



    @Override
    public String toString() {
        return "LISTA WSZYSTKICH KLAS: " +
                "LISTA KLAS" + studentClassList +
                ", STUDENCI W KLASIE" + studentToStudentClassList;
    }
}
