package school.view;

import school.SchoolController;
import school.SchoolMVC;
import school.controller.printer.PrinterController;
import school.controller.printer.PrinterMVC;
import school.controller.scanner.ScannerController;
import school.controller.scanner.ScannerMVC;
import school.model.School;

public class SchoolView implements SchoolMVC.View {

    private SchoolMVC.Model school = new School();
    private SchoolMVC.Controller schoolController;
    private ScannerMVC.Controller scannerController = new ScannerController();
    private PrinterMVC.Controller printerController = new PrinterController();

    public void start() {

        school = new School();
        schoolController = new SchoolController(school, scannerController, printerController);
        schoolController.manageSchool();
    }

}
