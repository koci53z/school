package school.model.grade;

import java.util.List;
import java.util.Optional;

public interface GradeInterface {
    public void addGrade(int subjectToStudentId, int newGrade);
    public void removeGradesOfStudent(List<Integer> subjectToStudentIdList);
    public List<Double> getStudentGradesOfSubject(int subjectToStudentId);
    public List<Integer> getStudentGradesIdOfSubject(int subjectToStudentId);
    public Optional<Grade> getStudentGradeById(int gradeId);
    public boolean correctGrade(int subjectToStudentId, int studentGradeId, int newGrade);
    public List<Grade> searchGradesToCorrect(int subjectToStudentId, int studentGradeId);
    public boolean gradeNeedToCorrect(int studentGradeId);
    public void resetDAO();

}
