package school;

import school.model.grade.Grade;
import school.model.grade.GradeDAO;
import school.model.student.StudentDAO;
import school.model.studentClass.StudentClassDAO;
import school.model.subject.SubjectDAO;

import java.util.List;

public interface SchoolMVC {

    interface Model {

        public void removeStudent(int studentId);

        public void removeSubject(int subjectId);

        public void addGrade(int gradeValue, int studentId, int subjectId);

        public double calculateSubjectMeanForStudent(int studentId, int subjectId);

        public double calcuateAllSubjectsMeanForStudent(int studentId);

        public List<Grade> getStudentBadGradesIdOfSubject(int studentId, int subjectId);

        public List<Grade> getStudentBadGradesIdOfAllSubjects(int studentId);

        public GradeDAO getGradeDAO();

        public StudentClassDAO getStudentClassDAO();

        public StudentDAO getStudentDAO();

        public SubjectDAO getSubjectDAO();

        public void reset();

    }

    interface Controller{
        public void manageSchool();

        public void endSchoolYear();
    }

    interface View{
        public void start();
    }
}
