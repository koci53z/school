package school.model.student;

import java.util.List;

public interface StudentInterface {

    public int addStudent(String studentName, String studentSurName);

    public void removeStudent(int studentId);

    public List<Student> getStudentsList();

    public void resetDAO();

}
