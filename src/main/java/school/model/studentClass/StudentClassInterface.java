package school.model.studentClass;

import java.util.Optional;

public interface StudentClassInterface {
    public void addStudentToClass(int studentId, int studentClassId);

    public void removeStudentClass(int studentClassId);

    public void removeStudentFromStudentClass(int studentId);

    public Optional<StudentClass> getStudentClassById(int studentClassId);

    public void addStudentClass( String className);

    public void resetDAO();

}
