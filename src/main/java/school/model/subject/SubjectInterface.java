package school.model.subject;

import school.model.SubjectToStudent;

import java.util.List;
import java.util.Optional;

public interface SubjectInterface {

    public void addStudentToSubject(int subjectId, int studentId);
    public void removeSubjectById (int subjectId);
    public void addSubject (String subjectName);
    public void removeStudentFromSubjects (int studentId);
    public List<Integer> getListOfStudentSubjects (int studentId);
    public List<Subject> getSubjectList();
    public List<SubjectToStudent> getSubjectsToStudentsList();
    public int getSubjectToStudentId(int studentId, int subjectId);
    public void removeSubjectFromSubjectsToStudentsList(int subjectId);
    public void resetDAO();


}
