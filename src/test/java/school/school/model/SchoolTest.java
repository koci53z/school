package school.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SchoolTest {

    School tested = new School();

    @BeforeEach
    void resetSingleton() {
        tested.reset();

        tested.getStudentClassDAO().addStudentClass("I");
        tested.getStudentClassDAO().addStudentClass("II");
        tested.getStudentClassDAO().addStudentClass("III");
        tested.getSubjectDAO().addSubject("Logika");
        tested.getSubjectDAO().addSubject("Greka");
        tested.getSubjectDAO().addSubject("Etyka");
        tested.studentDAO.addStudent("A", "B");
        tested.studentDAO.addStudent("C", "D");
        tested.studentDAO.addStudent("E", "F");
        tested.studentDAO.addStudent("G", "H");
        tested.studentDAO.addStudent("I", "J");
        tested.studentDAO.addStudent("K", "L");
        tested.studentDAO.addStudent("M", "N");
        tested.getStudentClassDAO().addStudentToClass(1, 1);
        tested.getStudentClassDAO().addStudentToClass(2, 1);
        tested.getStudentClassDAO().addStudentToClass(3, 1);
        tested.getStudentClassDAO().addStudentToClass(4, 2);
        tested.getStudentClassDAO().addStudentToClass(5, 2);
        tested.getStudentClassDAO().addStudentToClass(6, 3);
        tested.getStudentClassDAO().addStudentToClass(7, 3);

        tested.getSubjectDAO().addStudentToSubject(1, 1);
        tested.getSubjectDAO().addStudentToSubject(1, 2);
        tested.getSubjectDAO().addStudentToSubject(1, 3);

        tested.getSubjectDAO().addStudentToSubject(2, 1);
        tested.getSubjectDAO().addStudentToSubject(2, 2);
        tested.getSubjectDAO().addStudentToSubject(2, 3);

        tested.getSubjectDAO().addStudentToSubject(2, 4);
        tested.getSubjectDAO().addStudentToSubject(2, 5);
        tested.getSubjectDAO().addStudentToSubject(3, 4);
        tested.getSubjectDAO().addStudentToSubject(3, 5);

        tested.getSubjectDAO().addStudentToSubject(1, 6);
        tested.getSubjectDAO().addStudentToSubject(1, 7);
        tested.getSubjectDAO().addStudentToSubject(3, 6);
        tested.getSubjectDAO().addStudentToSubject(3, 7);

        tested.gradeDAO.addGrade(1, 3);
        tested.gradeDAO.addGrade(1, 4);
        tested.gradeDAO.addGrade(2, 4);
        tested.gradeDAO.addGrade(2, 5);
        tested.gradeDAO.addGrade(3, 5);
        tested.gradeDAO.addGrade(3, 5);

        tested.gradeDAO.addGrade(4, 2);
        tested.gradeDAO.addGrade(4, 2);
        tested.gradeDAO.addGrade(5, 5);
        tested.gradeDAO.addGrade(5, 5);
        tested.gradeDAO.addGrade(6, 4);
        tested.gradeDAO.addGrade(6, 4);

        tested.gradeDAO.addGrade(7, 4);
        tested.gradeDAO.addGrade(8, 6);
        tested.gradeDAO.addGrade(7, 3);
        tested.gradeDAO.addGrade(8, 5);

        tested.gradeDAO.addGrade(9, 6);
        tested.gradeDAO.addGrade(10, 6);
        tested.gradeDAO.addGrade(9, 5);
        tested.gradeDAO.addGrade(10, 5);

        tested.gradeDAO.addGrade(11, 4);
        tested.gradeDAO.addGrade(12, 1);
        tested.gradeDAO.addGrade(11, 4);
        tested.gradeDAO.addGrade(12, 5);

        tested.gradeDAO.addGrade(13, 6);
        tested.gradeDAO.addGrade(14, 5);
        tested.gradeDAO.addGrade(13, 1);
        tested.gradeDAO.addGrade(14, 5);

        tested.gradeDAO.addGrade(13, 1);
        tested.gradeDAO.addGrade(14, 1);
        tested.gradeDAO.addGrade(13, 6);
        tested.gradeDAO.addGrade(14, 4);
    }

    @Test
    void removeStudent() {
        tested.removeStudent(1);
        assertEquals(6, tested.getStudentDAO().getStudentsList().size());
        assertEquals(2, tested.getStudentClassDAO().getStudentsOfClassById(1).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(1).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(1).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(4).size());

        tested.removeStudent(2);
        assertEquals(5, tested.getStudentDAO().getStudentsList().size());
        assertEquals(1, tested.getStudentClassDAO().getStudentsOfClassById(1).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(2).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(2).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(5).size());

        tested.removeStudent(3);
        assertEquals(4, tested.getStudentDAO().getStudentsList().size());
        assertEquals(0, tested.getStudentClassDAO().getStudentsOfClassById(1).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(3).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(3).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(6).size());

        tested.removeStudent(4);
        assertEquals(3, tested.getStudentDAO().getStudentsList().size());
        assertEquals(1, tested.getStudentClassDAO().getStudentsOfClassById(2).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(4).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(7).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(9).size());

        tested.removeStudent(5);
        assertEquals(2, tested.getStudentDAO().getStudentsList().size());
        assertEquals(0, tested.getStudentClassDAO().getStudentsOfClassById(2).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(5).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(8).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(10).size());

        tested.removeStudent(6);
        assertEquals(1, tested.getStudentDAO().getStudentsList().size());
        assertEquals(1, tested.getStudentClassDAO().getStudentsOfClassById(3).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(6).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(11).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(13).size());

        tested.removeStudent(7);
        assertEquals(0, tested.getStudentDAO().getStudentsList().size());
        assertEquals(0, tested.getStudentClassDAO().getStudentsOfClassById(3).size());
        assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(7).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(12).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(14).size());

    }

    @Test
    void removeSubject() {
        tested.removeSubject(1);

        assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(1).size());
        assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(2).size());
        assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(3).size());
        assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(6).size());
        assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(7).size());

        assertEquals(2, tested.getSubjectDAO().getSubjectList().size());

        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(1).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(2).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(3).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(11).size());
        assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(12).size());
    }

    @Test
    void addGrade() {

        assertEquals(3.5, tested.calculateSubjectMeanForStudent(1, 1));
        assertEquals(2, tested.getGradeDAO().getStudentGradesOfSubject(1).size());
        tested.gradeDAO.addGrade(1, 5);
        assertEquals(4, tested.calculateSubjectMeanForStudent(1, 1));
        assertEquals(3, tested.getGradeDAO().getStudentGradesOfSubject(1).size());
        tested.gradeDAO.addGrade(1, 6);
        assertEquals(4.5, tested.calculateSubjectMeanForStudent(1, 1));
        assertEquals(4, tested.getGradeDAO().getStudentGradesOfSubject(1).size());

    }

    @Test
    void calculateSubjectMeanForStudent() {

        assertEquals(3.5, tested.calculateSubjectMeanForStudent(1, 1));
        assertEquals(2.0, tested.calculateSubjectMeanForStudent(1, 2));

        assertEquals(4.5, tested.calculateSubjectMeanForStudent(2, 1));
        assertEquals(5.0, tested.calculateSubjectMeanForStudent(2, 2));

        assertEquals(5.0, tested.calculateSubjectMeanForStudent(3, 1));
        assertEquals(4.0, tested.calculateSubjectMeanForStudent(3, 2));

        assertEquals(3.5, tested.calculateSubjectMeanForStudent(4, 2));
        assertEquals(5.5, tested.calculateSubjectMeanForStudent(4, 3));

        assertEquals(5.5, tested.calculateSubjectMeanForStudent(5, 2));
        assertEquals(5.5, tested.calculateSubjectMeanForStudent(5, 3));

        assertEquals(4.0, tested.calculateSubjectMeanForStudent(6, 1));
        assertEquals(3.5, tested.calculateSubjectMeanForStudent(6, 3));

        assertEquals(3.0, tested.calculateSubjectMeanForStudent(7, 1));
        assertEquals(3.75, tested.calculateSubjectMeanForStudent(7, 3));

    }

    @Test
    void calcuateAllSubjectsMeanForStudent() {

        assertEquals(2.75, tested.calcuateAllSubjectsMeanForStudent(1));
        assertEquals(4.75, tested.calcuateAllSubjectsMeanForStudent(2));
        assertEquals(4.5, tested.calcuateAllSubjectsMeanForStudent(3));
        assertEquals(4.5, tested.calcuateAllSubjectsMeanForStudent(4));
        assertEquals(5.5, tested.calcuateAllSubjectsMeanForStudent(5));
        assertEquals(3.75, tested.calcuateAllSubjectsMeanForStudent(6));
        assertEquals(3.375, tested.calcuateAllSubjectsMeanForStudent(7));

    }

    @Test
    void getStudentBadGradesIdOfSubject() {

        assertEquals(0, tested.getStudentBadGradesIdOfSubject(1, 1).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(1, 2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(2, 1).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(2, 2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(3, 1).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(3, 2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(4, 2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(4, 3).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(5, 2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(5, 3).size());
        assertEquals(0, tested.getStudentBadGradesIdOfSubject(6, 1).size());
        assertEquals(2, tested.getStudentBadGradesIdOfSubject(6, 3).size());
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(6, 3).get(0).grade);
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(6, 3).get(1).grade);
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 1).size());
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 1).get(0).grade);
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 3).size());
        assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 3).get(0).grade);

    }

    @Test
    void getStudentBadGradesIdOfAllSubjects() {

        assertEquals(0, tested.getStudentBadGradesIdOfAllSubjects(1).size());
        assertEquals(0, tested.getStudentBadGradesIdOfAllSubjects(2).size());
        assertEquals(0, tested.getStudentBadGradesIdOfAllSubjects(3).size());
        assertEquals(0, tested.getStudentBadGradesIdOfAllSubjects(4).size());
        assertEquals(0, tested.getStudentBadGradesIdOfAllSubjects(5).size());
        assertEquals(2, tested.getStudentBadGradesIdOfAllSubjects(6).size());
        assertEquals(2, tested.getStudentBadGradesIdOfAllSubjects(7).size());

    }

    @Test
    void correctGrade() {

        // printer and scanner in singleton

    }

    @Test
    void endSchoolYear() {

        // printer and scanner in singleton

    }

    @Test
    void getGradeDAO() {
        assertSame(tested.gradeDAO, tested.getGradeDAO());
    }

    @Test
    void getStudentClassDAO() {
        assertSame(tested.studentClassDAO, tested.getStudentClassDAO());
    }

    @Test
    void getStudentDAO() {
        assertSame(tested.studentDAO, tested.getStudentDAO());
    }

    @Test
    void getSubjectDAO() {
        assertSame(tested.subjectDAO, tested.getSubjectDAO());
    }
}