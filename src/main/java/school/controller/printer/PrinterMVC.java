package school.controller.printer;

public interface PrinterMVC {
    interface Controller {

        void println(Object object);

    }
}
