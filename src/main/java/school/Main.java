package school;

import school.view.SchoolView;

public class Main {

    public static void main(String[] args) {
        SchoolView schoolView = new SchoolView();
        schoolView.start();

    }

}
