package school.model.grade;

import org.junit.jupiter.api.Test;
import school.model.student.StudentDAO;
import school.model.subject.SubjectDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GradeDAOTest {

    @Test
    public void addGradeTest1() {

        GradeDAO gradeDAO = new GradeDAO();
        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);

        assertEquals (3, gradeDAO.gradeList.size());
        assertEquals(4, gradeDAO.gradeList.get(0).grade);
        assertEquals(5, gradeDAO.gradeList.get(1).grade);
        assertEquals(6, gradeDAO.gradeList.get(2).grade);
    }

    @Test
    public void removeGradeTest1() {

        GradeDAO gradeDAO = new GradeDAO();
        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);

        gradeDAO.addGrade(3, 4);
        gradeDAO.addGrade(3, 5);
        gradeDAO.addGrade(3, 6);

        List<Integer> list = new ArrayList<>();
        list.add(1);
        gradeDAO.removeGradesOfStudent(list);
        assertEquals(3, gradeDAO.gradeList.size());
        assertEquals(3, gradeDAO.gradeList.get(0).subjectToStudentId);
        assertEquals(3, gradeDAO.gradeList.get(1).subjectToStudentId);
        assertEquals(3, gradeDAO.gradeList.get(2).subjectToStudentId);
    }

    @Test
    public void getStudentGradesOfSubjectTest1 () {
        GradeDAO gradeDAO = new GradeDAO();
        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);

        gradeDAO.getStudentGradesOfSubject(1);
        assertEquals(3, gradeDAO.gradeList.size());
    }

    @Test
    public void getStudentGradesOfSubjectTest2 () {
        GradeDAO gradeDAO = new GradeDAO();
        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);

        assertEquals(0, gradeDAO.getStudentGradesOfSubject(2).size());
    }

    @Test
    public void getStudentGrades () {
        GradeDAO gradeDAO = new GradeDAO();
        StudentDAO studentDAO = new StudentDAO();
        SubjectDAO subjectDAO = new SubjectDAO();
        studentDAO.addStudent("A", "B");
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(3, 1);

        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);


        gradeDAO.addGrade(3, 4);
        gradeDAO.addGrade(3, 5);
        gradeDAO.addGrade(3, 6);

        gradeDAO.getStudentGradesOfSubject(1);

        assertEquals(6, gradeDAO.gradeList.size());
    }

    @Test
    public void getStudentGradesIdOfSubject () {
        GradeDAO gradeDAO = new GradeDAO();

        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);

        gradeDAO.getStudentGradesIdOfSubject(1);

        assertEquals(3, gradeDAO.gradeList.size());
    }

    @Test
    public void getStudentGradeById () {
        GradeDAO gradeDAO = new GradeDAO();

        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 1);
        gradeDAO.addGrade(1, 6);

    }

}