package school.model.studentClass;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentClassDAOTest {

    @Test
    public void addStudentClassTest() {

        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        assertNotNull(studentClassDAO.studentClassList);
    }

    @Test
    public void addStudentToClassTest1() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentToClass(1, 1);
        assertEquals(1, studentClassDAO.studentClassList.size());
        assertEquals(1, studentClassDAO.studentToStudentClassList.size());
    }

    @Test
    public void addStudentToClassTest2() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 2);
        studentClassDAO.addStudentToClass(2, 1);

        assertEquals(1, studentClassDAO.studentClassList.size());
        assertEquals(2, studentClassDAO.studentToStudentClassList.size());
    }

    @Test
    public void removeStudentFromClassTest1() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 1);
        studentClassDAO.addStudentToClass(3, 1);

        studentClassDAO.removeStudentFromStudentClass(1);

        assertEquals(2, studentClassDAO.studentToStudentClassList.size());
    }

    @Test
    public void removeClassTest1() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentClass("IVb");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 2);
        studentClassDAO.addStudentToClass(3, 1);

        studentClassDAO.removeStudentClass(1);
        assertFalse(studentClassDAO.getStudentClassById(1).isPresent());
        assertEquals(1, studentClassDAO.studentClassList.size());

    }


    @Test
    public void getStudentClassByIdTest1() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentClass("IVb");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 2);
        studentClassDAO.addStudentToClass(3, 1);

        assertEquals(studentClassDAO.studentClassList.get(1), studentClassDAO.getStudentClassById(2).get());

    }

    @Test
    public void getStudentsOfClassByIdTest1() {
        StudentClassDAO studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
        studentClassDAO.addStudentClass("IVb");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 2);
        studentClassDAO.addStudentToClass(3, 1);

        assertEquals(2, studentClassDAO.getStudentsOfClassById(1).size());
        assertEquals(1, studentClassDAO.getStudentsOfClassById(2).size());

    }

}