package school.model;

import school.controller.printer.PrinterController;
import school.controller.printer.PrinterMVC;
import school.controller.scanner.ScannerController;
import school.controller.scanner.ScannerMVC;
import school.model.grade.Grade;
import school.model.grade.GradeDAO;
import school.model.student.StudentDAO;
import school.SchoolMVC;
import school.model.studentClass.StudentClassDAO;
import school.model.subject.SubjectDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class School implements SchoolMVC.Model {

    private static School instance;

    public static School getInstance() {
        if (instance == null) {
            instance = new School();
        }
        return instance;
    }

    public GradeDAO gradeDAO = new GradeDAO();
    public StudentClassDAO studentClassDAO = new StudentClassDAO();
    public StudentDAO studentDAO = new StudentDAO();
    public SubjectDAO subjectDAO = new SubjectDAO();
    private PrinterMVC.Controller printerController = new PrinterController();

    @Override
    public void removeStudent(int studentId) {
        studentClassDAO.removeStudentFromStudentClass(studentId);
        gradeDAO.removeGradesOfStudent(subjectDAO.getListOfStudentToSubjectsId(studentId));
        subjectDAO.removeStudentFromSubjects(studentId);
        studentDAO.removeStudent(studentId);
    }

    @Override
    public void removeSubject(int subjectId) {
        studentDAO.getStudentsList().forEach(s -> {
            gradeDAO.removeGradesOfStudent(subjectDAO.subjectsToStudentsList.stream()
                    .filter(p -> p.studentId == s.studentId & p.subjectId == subjectId)
                    .map(p -> p.subjectToStudentId)
                    .collect(Collectors.toList()));
        });
        subjectDAO.removeSubjectFromSubjectsToStudentsList(subjectId);
        subjectDAO.removeSubjectById(subjectId);
    }

    @Override
    public void addGrade(int gradeValue, int studentId, int subjectId) {
        if (subjectDAO.getSubjectToStudentId(studentId, subjectId) != -1) {
            gradeDAO.addGrade(subjectDAO.getSubjectToStudentId(studentId, subjectId), gradeValue);
        }
    }

    @Override
    public double calculateSubjectMeanForStudent(int studentId, int subjectId) {
        int subjectToStudentId;
        subjectToStudentId = subjectDAO.getSubjectToStudentId(studentId, subjectId);
        if (subjectToStudentId != -1) {
            long count = gradeDAO.gradeList.stream()
                    .filter(p -> p.subjectToStudentId == subjectToStudentId)
                    .count();
            if (!gradeDAO.gradeList.isEmpty()) {
                double sum = gradeDAO.gradeList.stream()
                        .filter(p -> p.subjectToStudentId == subjectToStudentId)
                        .map(p -> p.grade)
                        .reduce((x, y) -> x + y)
                        .get();
                return sum / (double) count;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    @Override
    public double calcuateAllSubjectsMeanForStudent(int studentId) {
        List<Integer> subjectsOfStudent;

        subjectsOfStudent = subjectDAO.subjectsToStudentsList.stream()
                .filter(p -> p.studentId == studentId)
                .map(p -> p.subjectId)
                .collect(Collectors.toList());

        List<Double> meansOfStudent = new ArrayList<>();

        subjectsOfStudent.forEach(it -> {
            meansOfStudent.add(calculateSubjectMeanForStudent(studentId, it));
        });
        return meansOfStudent.stream().reduce((a, b) -> a + b).get() / meansOfStudent.size();
    }

    @Override
    public List<Grade> getStudentBadGradesIdOfSubject(int studentId, int subjectId) {

        List<Grade> studentBadGrades = new ArrayList<>();

        List<Integer> studentGradesId = gradeDAO
                .getStudentGradesIdOfSubject(subjectDAO.getSubjectToStudentId(studentId, subjectId));

        studentGradesId.forEach(it -> {
            if (gradeDAO.getStudentGradeById(it).get().grade == 1) {
                studentBadGrades.add(gradeDAO.getStudentGradeById(it).get());
            }

        });
        return studentBadGrades;

    }

    @Override
    public List<Grade> getStudentBadGradesIdOfAllSubjects(int studentId) {

        List<Grade> studentBadGrades = new ArrayList<>();

        subjectDAO.getListOfStudentSubjects(studentId).forEach(it -> {

            studentBadGrades.addAll(getStudentBadGradesIdOfSubject(studentId, it));
        });
        return studentBadGrades;
    }

    public GradeDAO getGradeDAO() {
        return gradeDAO;
    }

    public StudentClassDAO getStudentClassDAO() {
        return studentClassDAO;
    }

    public StudentDAO getStudentDAO() {
        return studentDAO;
    }

    public SubjectDAO getSubjectDAO() {
        return subjectDAO;
    }

    public void reset() {
        gradeDAO.resetDAO();
        studentClassDAO.resetDAO();
        studentDAO.resetDAO();
        subjectDAO.resetDAO();
    }


}
