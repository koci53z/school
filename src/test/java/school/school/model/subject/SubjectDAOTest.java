package school.model.subject;

import org.junit.jupiter.api.Test;
import school.model.student.StudentDAO;

import static org.junit.jupiter.api.Assertions.*;

class SubjectDAOTest {

    public SubjectDAO subjectDAO = new SubjectDAO();
    public Subject subject = new Subject();
    public StudentDAO studentDAO = new StudentDAO();

    @Test
    public void addStudentToSubject() {
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(1, 2);
        assertNotNull(subjectDAO.subjectsToStudentsList);
    }

    @Test
    public void removeSubjectId() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.removeSubjectById(2);
        assertEquals(2, subjectDAO.subjects.size());
    }

    @Test
    public void addSubject() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        assertEquals(3, subjectDAO.subjects.size());

    }

    @Test
    public void removeStudentFromSubjects() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(1, 2);
        subjectDAO.removeStudentFromSubjects(2);
        assertEquals(1, subjectDAO.subjects.size());
    }

    @Test
    public void getListOfStudentSubjects() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(2, 1);
        subjectDAO.getListOfStudentSubjects(1);
        assertEquals(2, subjectDAO.getListOfStudentSubjects(1).size());

    }

    @Test
    public void getSubjectList() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.addSubject("Język Polski");
        subjectDAO.addSubject("Łacina");
        subjectDAO.addSubject("Greka");

        assertEquals(subjectDAO.subjects, subjectDAO.getSubjectList());
        assertEquals(6, subjectDAO.getSubjectList().size());
    }

    @Test
    public void getSubjectsToStudentList() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.addSubject("Język Polski");
        subjectDAO.addSubject("Łacina");
        subjectDAO.addSubject("Greka");
        subjectDAO.addStudentToSubject(0, 1);
        subjectDAO.addStudentToSubject(1, 2);
        subjectDAO.addStudentToSubject(2, 3);
        subjectDAO.addStudentToSubject(3, 4);
        subjectDAO.addStudentToSubject(4, 5);

        assertEquals(subjectDAO.subjectsToStudentsList, subjectDAO.getSubjectsToStudentsList());
        assertEquals(5, subjectDAO.getSubjectsToStudentsList().size());
    }

    @Test
    public void getSubjectsToStudentId() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.addSubject("Język Polski");
        subjectDAO.addSubject("Łacina");
        subjectDAO.addSubject("Greka");
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(2, 2);
        subjectDAO.addStudentToSubject(3, 3);
        subjectDAO.addStudentToSubject(4, 4);
        subjectDAO.addStudentToSubject(5, 5);

        assertEquals(5, subjectDAO.getSubjectToStudentId(5, 5));

    }

}