package school.controller.scanner;

public interface ScannerMVC {
    interface Controller {

        int nextInt();

        int pickOption();

        String nextString();
    }
}
