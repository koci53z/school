package school.model.grade;

public class Grade {

    public int studentGradeId;
    public double grade;
    public int subjectToStudentId;
    public boolean isCorrected;

}
