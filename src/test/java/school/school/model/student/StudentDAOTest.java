package school.model.student;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentDAOTest {

    public StudentDAO studentDAO = new StudentDAO();

    @Test
    public void addStudent() {
        studentDAO.addStudent("Jan",  "Kowalski");
        assertNotNull(studentDAO.students);
    }

    @Test
    public void removeStudent() {
        studentDAO.addStudent("Jan",  "Kowalski");
        assertEquals(1, studentDAO.students.size());
        studentDAO.removeStudent(1);
        assertEquals(0, studentDAO.students.size());
    }

    @Test
    public void addAndRemoveStudents(){
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Jan",  "Kowalski");
        assertEquals(4, studentDAO.students.size());
        studentDAO.removeStudent(2);
        studentDAO.removeStudent(3);
        assertEquals(2, studentDAO.students.size());
    }

    @Test
    public void isStudentPresent () {
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Jan",  "Kowalski");
        assertEquals(1, studentDAO.students.stream().filter(index -> index.studentId == 3).count());
    }


    @Test
    public void getStudentsListTest1(){
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Janina",  "Kowalska");
        studentDAO.addStudent("Jarek",  "Kowalski");
        studentDAO.addStudent("Janusz",  "Kowalski");

        assertEquals(studentDAO.students,studentDAO.getStudentsList());
        assertEquals(4,studentDAO.students.size());

    }

}